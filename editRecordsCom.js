$(document).ready(function () {
	var options = {
		    ajaxPrefix: ''
		};
    $('#insert_buttonR').click(function () {
    
    		new Dialogify('ComAdjun/add-form-comAudit.php', options)
                .title('Crear Nuevo Comentario')
                .buttons([
                    {
                        text: 'Cancel',
                        click: function (e) {
                            console.log('cancel click');
                            this.close();
                        }
                    },
                    {    
                        text: 'Agregar',
                        type: Dialogify.BUTTON_PRIMARY,
                        click: function (e) {
                            var ID_prolin = document.getElementById("ID_prolin").value;
                            var content = document.getElementById("new_content").value;
                            $.ajax
                                    ({
                                        type: 'post',
                                        url: 'ComAdjun/comment-add-comAudit.php',
                                        data: {
                                            insert_row: 'insert_row',
                                            ID_prolin: ID_prolin,
                                            content: content
                                        },
                                        success: function (response) {
                                            if (response != "")
                                            {
                                                var id = response;
                                                var row = "<div id='ID_prolin_val" + id + "'>" +  ID_prolin + "</div><div id='website_val" + id + "'>" +  "</div><div id='content_val" + id + "'>" + content + "</div><input type='button' class='edit_button' id='edit_button" + id + "' value='Editar' onclick='edit_row(" + id + ");'/>";
                                                $('#container').append(row);
                                            }

                                          location.reload();
                                        }
                                    });
                            this.close();
                        }
                    }
                ]).show();
    });
});
function edit_rowR(id)
{
            var content = $.trim(document.getElementById("content_valR" + id).innerHTML);
            	localStorage.setItem('contentR',content);			
		var options = {
		    ajaxPrefix: '',			 
		};               
    new Dialogify('ComAdjun/edit-form-comAudit.php', options)
            .title('Editar Comentario')
            .buttons([
                {
                    text: 'Cancelar',
                    click: function (e) {
                        console.log('cancel click');
                        this.close();
                    }
                },
                {
                    text: 'Editar',
                    type: Dialogify.BUTTON_PRIMARY,
                    click: function (e) {
                        var content = $('#edit_contentR').val();
                        $.ajax
                                ({
                                    type: 'post',
                                    url: 'ComAdjun/comment-edit-comAudit.php',
                                    data: {
                                        edit_row: 'edit_row',
                                        id: id,
                                        content: content
                                    },
                                    success: function (response) {
                                        if (response == "success")
                                        {
                                        document.getElementById("contentR_val" + id).innerHTML = content;
                                        }
                                        location.reload();
                                    }                                    
                                });                                                                 
                        this.close();
                    }
                }
            ]).show();			
    $('#edit_contentR').val(content);       
}


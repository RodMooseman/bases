<!--Adjunto18-->
<div class="col-md-3"></div>
	<div class="col-md-10 well">
		
		<hr style="border-top:1px dottec #ccc;">
		<form class="form-inline" method="POST" action="Adjuntos/Ad_fileAudit/upload.php" enctype="multipart/form-data">
			<input class="form-control" type="file" name="upload"/>
			<input type="hidden" name="fileAudit" id="fileAudit" value="<?php echo $Sheet ?>"/>
			<input type="hidden" name="ID_prolinR" id="ID_prolinR" value="<?php echo $ID_prolin ?>"/>
			<button type="submit" class="btn btn-success form-control" name="submitR"><span class="glyphicon glyphicon-upload"></span> Upload</button>
		</form>
		<br /><br />
		<table class="table table-bordered">
			<thead class="alert-warning">
				<tr>
					<th>Nombre</th>
					<th>Acci&oacute;n</th>
				</tr>
			</thead>
			<tbody class="alert-success">
				<?php
					require 'Adjuntos/Ad_fileAudit/connection.php';
					$row = $conn->query("SELECT * FROM ad_fileAudit where ID_prolin =" . $ID_prolin) or die(mysqli_error());
					while($fetch = $row->fetch_array()){
				?>
					<tr>
						<?php 
							$nameR = $fetch['file'];
							$nameUNR = urlencode($nameR);
						?>
						<td><?php echo $fetch['name']?></td>
						<td><a href="Adjuntos/Ad_fileAudit/download.php?file=<?php echo $nameUNR?>" class="btn btn-primary"><span class="glyphicon glyphicon-download"></span> Download</a></td>
					</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>		
<!--Adjunto18-->